# Gitlab Issue Chains

Improved Issue Templates. Templates additionally serves as documentation. Allows fast creation of Gitlab issues directly from documentation. 

For a sample project see: https://gitlab.com/t.feichtinger/gitlab-issue-chains-sample

## Usage

WIP

## Motivation

Back at my old company we documented our most common processes in what we called 'issue chains'. You can think of it as a step-by-step guide of 'stuff-to-do' for a given project type. It all started out as a simple text file with numbered 'steps' that needed to be performed. Those files grew as more and more documentation was added and it got rather confusing. Also, updating the step numbers when a new step was added was also error-prone and cumbersome. Due to the nature of our projects you would be forced to stop work while waiting for more data and pick up where we left off a couple days/weeks later. So we needed a better way to document the progress. We chose to use Gitlab issues for that: when you are about to perform a 'step', you would create a new issue. You were then able to document progress via issue state and comments. 

Only creating the issues remained to be solved. Gitlab has issue templates, but we needed something 'better/different':
* we wanted to automatically number the issues
    * order is important
    * if a new step was added in the middle, all other steps should automatically be updated
* we wanted an easy way to navigate through the templates and treat them more as documentation
* it should be easy to add/modify steps

This project is the solution I came up with to fulfill all our requirements: 

1. Document the steps/issues as markdown files in a git repo
	- markdown is a somewhat simple format so everyone is able to write 'issue-chains'
	- we get versioning by putting it in a git repo
2. Use docsify to render those markdown in the browser and sprinkle some custom javascript on top of it to talk to the Gitlab API and create an issue in the correct Gitlab project
	- docsify consumes markdown files and generates a nice looking website on the fly
3. Use Gitlab CI to generate all necessary files and use Gitlab Pages to host it

## Testing locally

To test locally you will need to install docsify:
```
npm i docsify-cli -g
```

Before you can serve with docsify, you need to generate the required files using the `chains-builder.py` script:
```
cd <working-dir-with-yml-files>
python chains-builder.py
```

This will create a `public` folder which can be hosted locally:

```
docsify serve public
```