import argparse
import glob
import os
from shutil import copy2, copytree, rmtree
import shutil
from subprocess import check_output

import pystache
import yaml

public_dir = 'public'


class IssueChainGenerator:
    def __init__(self, title, description, label, ordered, issues):
        self.title = title
        self.description = description
        self.label = label
        self.ordered = ordered
        self.steps = issues
        self.counter = 0
        self.out_path = os.path.join(
            public_dir, title.replace(" ", "").lower())
        self.sidebar_path = os.path.join(self.out_path, '_sidebar.md')

    def get_step_id(self, idx, parents=[]):
        if not parents or parents[-1] == '_':
            return str(idx)
        else:
            if '_' in parents:
                start = max(loc for loc, val in enumerate(
                    parents) if val == '_') + 1
            else:
                start = 0
            if idx is None:
                return '.'.join(parents[start:])
            else:
                return '.'.join(parents[start:]) + '.' + str(idx)

    def generate_index_file(self):
        basedir = os.path.dirname(os.path.realpath(__file__))
        mustache_file = os.path.join(basedir, 'index.mustache')
        if not os.path.isfile(mustache_file):
            print('Missing {}!'.format('index.mustache'))
            exit(1)

        with open(mustache_file, 'r', encoding='utf-8') as f:
            content = pystache.render(
                f.read(), {'title': self.title, 'label': self.label})
            with open(os.path.join(self.out_path, 'index.html'), 'w', encoding='utf-8') as index_file:
                index_file.write(content)

    def generate_home_md(self):
        with open(os.path.join(self.out_path, 'home.md'), 'w', encoding='utf-8') as home_file:
            home_file.writelines(['# ', self.title, '\n\n', self.description])

    def generate(self):
        # clean output directory
        if os.path.isdir(self.out_path):
            rmtree(self.out_path)
        os.makedirs(self.out_path)

        self.generate_index_file()
        self.generate_home_md()

        with open(self.sidebar_path, 'w+', encoding='utf-8') as sidebar:
            self.generate_steps(self.steps, sidebar, self.ordered)

    def generate_steps(self, steps, sidebar_file, ordered=True, parents=()):
        for idx, step in enumerate(steps):

            # step is basically the index of the issue in the chain
            step_idx = str(idx+1)
            step_id = self.get_step_id(step_idx, parents)
            step_name = step_id

            if (ordered):
                title = '{} {}'.format(step_name, step['title'])
            else:
                title = '{}'.format(step['title'])

            md_file = step['file']
            if os.path.isfile(os.path.join(self.out_path, md_file)):
                md_file = '{}_{}'.format(
                    step_id.replace('.', '-'), step['file'])

            indent_level = '  ' * len(parents)
            toc_entry = '{}* [{}]({})'.format(indent_level, title, md_file)
            sidebar_file.write(toc_entry + "\n")
            print(toc_entry)

            file_path = os.path.join(args.docs, step['file'])
            if not os.path.isfile(file_path):
                raise FileNotFoundError('Missing "{0}"!'.format(file_path))

            destination_path = os.path.join(self.out_path, md_file)
            shutil.copyfile(file_path, destination_path)

            if 'ordered' in step:
                childNumbering = step['ordered']
            else:
                childNumbering = self.ordered

            if 'steps' in step:
                self.generate_steps(
                    step['steps'], sidebar_file, childNumbering, parents=parents + (step_idx, ))


def read_yml(tickets):
    with open(tickets, 'r', encoding='utf-8') as stream:
        definition = yaml.safe_load(stream)
    return definition


def create_home(chain_definitions):
    print('\nGenerating index for {} ticket lists...'.format(len(chain_definitions)))

    basedir = os.path.dirname(os.path.realpath(__file__))
    mustache_path = os.path.join(basedir, 'home.mustache')
    out_path = os.path.join(public_dir)

    if not os.path.isfile(mustache_path):
        print('Missing {}!'.format('index.mustache'))
        exit(1)

    chains = []
    for chain in chain_definitions:
        definition = read_yml(chain)
        url_path = definition['title'].replace(" ", "").lower()
        chains.append({'title': definition['title'],
                      'url_path': f'{url_path}/index.html'})

    with open(mustache_path, 'r', encoding='utf-8') as mustache_file:
        content = pystache.render(
            mustache_file.read(), {'chains': chains})
        with open(os.path.join(out_path, 'index.html'), 'w', encoding='utf-8') as index_file:
            index_file.write(content)


def create_chain(args):
    if not os.path.isfile(args.definition):
        print('Missing {}!'.format(args.definition))
        exit(1)
    chain_definition = read_yml(args.definition)

    if 'title' not in chain_definition:
        print('Issue chain title missing!')
        exit(1)
    title = chain_definition['title']

    if 'description' in chain_definition:
        description = chain_definition['description']
    else:
        description = ''

    if 'label' in chain_definition:
        label = chain_definition['label']
    else:
        # if no special label was specified, use the title  of the chain
        label = chain_definition['title']

    if 'ordered' in chain_definition:
        ordered = chain_definition['ordered']
    else:
        ordered = True

    gen = IssueChainGenerator(
        title, description, label, ordered, chain_definition['steps'])
    gen.generate()


def main(args):
    issue_chain_definitions = []
    if args.definition is None:
        # optional arg was not specified, look for all .yml files
        issue_chain_definitions.extend(glob.glob(r"*.yml"))
    else:
        issue_chain_definitions.append(args.definition)

    for chain_definition in issue_chain_definitions:
        args.definition = chain_definition
        create_chain(args)

    create_home(issue_chain_definitions)


if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument('--docs', default='docs',
                    help='the directory containing the .md-files')
    ap.add_argument('--gitlab_url', help='the url to the gitlab instance')
    ap.add_argument(
        '--definition', help="the name of the yml file which contains the issue chain definition", required=False)
    #ap.add_argument('--repo', help='the url of the gitlab repo', required=True)
    ap.add_argument('--no-label-generation', help='Whether to generate group labels or not',
                    dest='generate_labels', action='store_false')
    ap.add_argument('--omit-numbering', help='if this argument is used then tickets will not be numbered',
                    dest='omit_numbering', action='store_true')
    ap.add_argument('--branch', default='master', help='the git branch')
    args = ap.parse_args()

    main(args)
